# Científico de datos en NEQUI: Prueba Técnica </em>
Este proyecto se desarrolló como prueba técnica para la vacate de Científico de datos en Nequi. El propósito de esta prueba es verificar tus conocimientos en el campo del modelado analítico de datos.

## Pasos para la configuración
Para instalar dependencias ejecutando el comando: pip install --upgrade -r requirements.txt

#### Importante
Para este proyecto se eligió trabajar con el lenguaje Python implementando librerías de machine learning como keras y tensor flow.
Se implementaron tres diferentes modelos cuyos algoritmos están en funciones almacenadas en el script "Modelo.py" el cual es importado al comienzo del script "Main.py".

****

## Descripción del proyecto
Este proyecto se puede abordar en tres bloques que se componen por:
1- El análisis exploratorio de los datos
2- La elaboración de los modelos

  - Random Forest
  - Análisis de patrones transaccionales 
  - Redes Neuronales

3- La elección del mejor modelo
4- Arquitectura ideal
5- Macro operativo


****

## Analisis exploratorio de los datos
El dataset utilizado para esta prueba "sample_data_0006_part_00.parquet", contiene 10'758.418 filas y 9 columnas, las cuales se presentan a continuación.


|       name         |                                     Descripcion                                        | tipo  |
|--------------------|----------------------------------------------------------------------------------------|-------|
| _id                | identificador único del registro                                                       |  str  |
| merchant_id        | código único del comercio o aliado                                                     |  str  |
| subsidiary         | código único de la sede o sucursal                                                     |  str  |
| transaction_date   | fecha de transacción en el Core financiero                                             | date  |
| account_number     | número único de cuenta                                                                 |  int  |
| user_id            | código único del usuario dueño de la cuenta desde donde se registran las transacciones |  str  |
| transaction_amount | monto de la transacción (en moneda ficticia)                                           | float |
| transaction_type   | naturaleza de la transacción (crédito o débito)                                        |  str  |


Se encontró que no hay registros NA o null, al mismo tiempo se validó que cada columna tuviera el tipo de datos correspondiente.

Se decidió prescindir para el análisis de las variables _id, account_number y subsidiary ya que esta primera es un registro único para cada una de las transacciones y no aportará nada de información a los modelos de clasificación, la segunda debido a que está estrechamente correlacionada a la variable [usuar_id] y posteriormente en el desarrollo del código cuando se procede a agrupar las variables esta pierde relevancia frente a la variable [user_id] y la tercera debido a que el tipo de modelo que se eligió necesariamente utiliza variables categóricas como insumo y esta variable cuenta con un poco más de 16.000 categorías lo que hace que el modelo no pueda ejecutarse fácilmente.

Adicionalmente se agregó una columna llamada [Transactions] con un escalar igual a 1 indicando que cada fila es una transacción.
Por lo tanto el data frame se trabajara como sigue:



|       name         |                                     Descripcion                                        | tipo  |
|--------------------|----------------------------------------------------------------------------------------|-------|
| merchant_id        | código único del comercio o aliado                                                     |  str  | 
| transaction_date   | fecha de transacción en el Core financiero                                             | date  | 
| user_id            | código único del usuario dueño de la cuenta desde donde se registran las transacciones |  str  | 
| transaction_amount | monto de la transacción (en moneda ficticia)                                           | float | 
| transaction_type   | naturaleza de la transacción (crédito o débito)                                        |  str  |
| Transactions       | escalar                                                                                |  str  |


Se pudo observar que la variable [merchant_id] tiene solo tres categorías como se ve en la siguiente imagen.

![](docs/Merchant_id.png)

También se pudo observar que la variable [transaction_type] tiene solo tres categorías como se ve en la siguiente imagen.

![](docs/Transaction_type.png)


El data frame contiene transacciones diarias hechas desde el 01/ene/2021 hasta el 30/nov/2021.
Esta información se agrupó bajo las caracteristicas de las variables [user_id], [merchant_id], [transaction_type] y [transaction_date],
teniendo en cuenta dos posibles escenarios.

1- Considerando las transacciones hechas por el mismo usuario dentro de un rango de 24 horas, es decir, si la primer transacción respecto a la segunda se hizo en un rango de tiempo inferior a 24 horas se agrupó y se sumaron las variables [transaction_amount] y [Transactions]

2- Considerando las transacciones hechas por el mismo usuario dentro de un rango de 24 horas y además encadenando la hora de cada una de las transacciones a la siguiente, es decir, si la primer transacción respecto a la segunda se hizo en un rango de tiempo inferior a 24 horas, la segunda transacción respecto a la tercera se hizo en un rango de tiempo inferior a 24 horas, pero la primera y la tercera transacción no fueron hechas dentro de un rango de 24 horas se agruparon las tres transacciones y se sumaron las variables [transaction_amount] y [Transactions]. 
Esto último debido a que el usuario que realizó las transacciones está haciendo cada una de estas con una diferencia en tiempo menor al rango de 24 entre transacciones, por lo tanto deben considerarse en un mismo grupo.


****

## Modelos

Antes de plantear los modelos se determinaron mediante técnicas estadísticas los que en adelante serán considerados como comportamientos atípicos en relación al número de transacciones y la cantidad de dinero transada en cada movimiento. 
Para esto se asume que la variables [transaction_amount] y [Transactions] tienen una distribución normal.

Se considera normal todos los valores que están dentro de la campana de Gauss con una probabilidad del 95%, es decir, se compararon los valores de estas variables y solo se consideró normal lo que no superará el valor de la media más 3 desviaciones.

Siendo esto último 6.7 transacciones y $1230 unidades monetarias, por lo tanto, todas las transacciones que superen estos valores son considerados atípicos y esta decisión se almacena en una nueva columna llamada [Decision].

Para cada uno de los modelos se creó una función que tiene como insumo el data frame llamado "results" el cual contiene la información agrupada y organizada.

### Random Forest

Se implementó un algoritmo de random forest donde se dividió el dataframe en un conjunto de entrenamiento y prueba mediante la función "train_test_split" de la librería "sklearn" con una proporción de datos 75-25. 

Se entrenó el modelo utilizando el conjunto de datos "results" donde ya se identificó previamente las transacciones que corresponden a fraccionamiento transaccional. Finalmente, se generó una clasificación la cual se comparó con el conjunto y_test cuya predicción se definió anteriormente, la cual arrojó los siguientes resultados.

### Análisis de patrones transaccionales 

Se hizo una búsqueda en la literatura relacionada con el comportamiento denominado fraccionamiento transaccional y se encontró que una de las técnicas más utilizadas para detectar este comportamiento en operaciones financieras es el Análisis de patrones transaccionales.

Se implementó un modelo sacado de la literatura donde se aplica una técnica de agrupamiento (en este caso, K-means) para agrupar transacciones similares en clusters. Los clusters se agregan al Data Frame "results" como una nueva columna llamada "Cluster".

Se identifican patrones de comportamiento inusual en el cluster 4 y se guardan en un DataFrame llamado "anomalies", luego se realizan algunos análisis estadísticos en los datos de anomalías, incluido el cálculo de un valor z-score y un valor p que se utiliza para determinar si los datos son estadísticamente significativos.

Este modelo arrojó los siguientes resultados.

### Redes Neuronales

Como otra alternativa, se implementó un modelo de Redes Neuronales de dos capas ocultas y una función de activación ReLU para clasificar los comportamientos normales y anormales, con el fin de tener más alternativas y elegir el mejor modelo para clasificar a los usuarios que estén o no implementando la práctica de fraccionamiento transaccional.

Para este modelo se implementó la función "Sequential" de la librería "Keras", se entrenó el modelo utilizando el conjunto de datos "results" donde ya se identificó previamente las transacciones que corresponden a fraccionamiento transaccional. Finalmente, se generó una clasificación la cual se comparó con el conjunto y_test cuya predicción se definió anteriormente, la cual arrojó los siguientes resultados.

****

# Eleccion del modelo

Para la elección del mejor modelo se construyó una tabla con los resultados de "Acuracy" de cada uno de los modelos y de igual manera se compararon las matrices de confusión de los tres modelos como se presenta a continuación.

Es fácil observar que el modelo que mejor ajustó los datos es el modelo de "Redes Neuronales" ya que tiene un acuracy de 1 lo que indica una precisión casi perfecta al momento de clasificar. 
Este resultado se puede corroborar comparando la matriz de confusión ya que para el modelo de "Redes Neuronales" indica que clasificó bien la totalidad de los datos de prueba. 

|       Modelo       |     Accuracy     |
|--------------------|------------------|
|   Random Forest    |    0.99999869    |
|  Redes Neuronales  |        1         |
|Análisis de patrones|    0.92230320    |


****

# Arquitectura ideal

La arquitectura ideal para desplegar esta propuesta es la siguiente

1- Se debe extraer la información mediante un RPA, conexión mediante una Api o cualquier metodología elegida por el departamento.

2- Idealmente esta extracción se debe hacer diariamente, consolidando las transacciones hechas a diario por los usuarios en una tabla con toda la información histórica.

3- Mediante un script de python es posible conectarse con algún sistema de gestión de bases de datos relacionales o no relacionales como SQL Server y después de procesar la información almacenarla en una tabla nutriendo sus respectivos maestros.

Estos maestros contienen cada una de las categorías de las variables encontradas en el dataset, con un consecutivo único que permite hacer las relaciones entre tablas óptimamente. 

!!! info
    Para efectos de demostración se agregó un script llamado "Conexion.py" con un ejemplo de cómo sería este desarrollo.

4- Del mismo modo se realiza una conexión con algún sistema de gestión de bases de datos y se extrae la información histórica de la tabla donde se almacenó anteriormente, aplicar las transformaciones, implementar los modelos y finalmente hacer la carga de la clasificación en otra tabla. 

5- Este proceso de clasificación debe implementarse mínimo una vez al mes, pero todo depende de la cantidad de datos sospechosos que se encuentren mediante esta metodología para que puedan ser gestionados ágilmente y no se representen los casos.

Para implementar esta propuesta es necesario contar con una máquina que cuente como mínimo con las siguientes características:

Processor	Intel(R) Core(TM) i5-9750H CPU @ 2.60GHz   2.59 GHz
Installed RAM	16.0 GB
System type	64-bit operating system, x64-based processor

****

# Macro operativo

Mediante la implementación de estos modelos y la escogencia del mejor clasificador de usuarios con un presunto comportamiento de fraccionamiento transaccional, es posible tener información de un conjunto de clientes sospechosos que deben ser analizados más a detalle. 

Un par de ejemplos de posibles análisis podrían ser, revisar el comportamiento histórico de cada uno de estos clientes o un análisis de redes transaccionales para determinar si tiene este usuario tiene relación con otros usuarios cuya comportamiento sea similar y compilar todas las evidencias para crear un caso donde se exponga este comportamiento inusual y finalmente escalar el caso a las autoridades ya que ellos son los encargados de determinar si el cliente está cometiendo un delito o no lo está haciendo. 







