def RandomForest(results, Mes_Actual):
    
    ''' Modelo Random Forest '''        
    
    results['Decision'] = results['Decision'].astype('category')

    # Entrenar el modelo
    
    X = results.iloc[:, :7].values
    Y = results.iloc[:, 7].values
    
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size = 0.25, random_state = 0)

    # Ajustar el clasificador  Random Forest en el Conjunto de Entrenamiento

    classifier = RandomForestClassifier(n_estimators = 100, criterion = "entropy", random_state = 0)
    classifier.fit(X_train, y_train)
    
    # Predicción de los resultados con el Conjunto de Testing
    y_pred  = classifier.predict(X_test)
    
    # Elaborar una matriz de confusión
    
    cm = confusion_matrix(y_test, y_pred)
    Accuracy = accuracy_score(y_test, y_pred)
    #Precision = precision_score(y_test, y_pred)
    #Recall = recall_score(y_test, y_pred)
    #F1_Score = f1_score(y_test, y_pred)
    
    #y_pred  = classifier.predict(Mes_Actual)

    return Accuracy, cm, y_pred


def Analisis_Patrones(result, Fecha_corte):

    ''' Análisis de patrones transaccionales '''   

    results = result.copy()
    
    results = results[results['transaction_date'] >=  Fecha_corte]#fn_fechas(date.today())[1]]  

    scaler = StandardScaler()
    results['transaction_amount'] = scaler.fit_transform(results['transaction_amount'].values.reshape(-1, 1))
    results['Transactions'] = scaler.fit_transform(results['Transactions'].values.reshape(-1, 1))
    
    # Establecer umbral para transacciones anómalas
    
    Trans_atipical = np.mean(results['Transactions']) + (np.std(results['Transactions']) * 3) # Calculamos la media más 3 desviaciones
    Amount_atipical = np.mean(results['transaction_amount']) + (np.std(results['transaction_amount']) * 3)
    
    # Separo la información del mes actual para clasificarla posteriormente con el modelo
        
    relevant_vars = ['merchant_id', 'transaction_type', 'transaction_amount', 'Transactions']
    results = results[relevant_vars]
    results[['merchant_id', 'transaction_type']] = results[['merchant_id', 'transaction_type']].astype('category')    
    results = pd.get_dummies(results)

    results['Decision'] = np.where((results['transaction_amount'] > Amount_atipical) & (results['Transactions'] > Trans_atipical) , 1, 0)

    kmeans = KMeans(n_clusters=5)
    kmeans.fit(results.iloc[:, :7])
    
    results['Cluster'] = kmeans.labels_
    
    # identificar patrones de comportamiento inusual
    anomalies = results[results['Cluster'] == 4]
    
    # análisis estadísticos
    mean_anomalies = anomalies['transaction_amount'].mean()
    std_anomalies = anomalies['transaction_amount'].std()
 
    results['Decision_cluster'] = np.where((results['Cluster'] == 4) & (1 - norm.cdf((results['transaction_amount'] - mean_anomalies) / std_anomalies)) > 0.5, 1, 0) 
    y_pred = results['Decision_cluster']
    y_test = results['Decision']
    
    cm = confusion_matrix(y_test, y_pred)
    Accuracy = accuracy_score(y_test, y_pred)
    #Precision = precision_score(y_test, y_pred)
    #Recall = recall_score(y_test, y_pred)
    #F1_Score = f1_score(y_test, y_pred)

    return Accuracy, cm, y_pred
        

def Red_Neuronal(results, Mes_Actual):

    ''' Redes neuronales '''   
    
    # Crear modelo de red neuronal
    
    model = Sequential()
    model.add(Dense(32, input_dim=results.shape[1], activation='relu'))
    model.add(Dense(16, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam')
    
    # Entrenar el modelo
    
    X_train = results.values
    y_train = results.iloc[:, 7].values #np.where((results['transaction_amount'] > Amount_atipical) & (results['Transactions'] > Trans_atipical) , 1, 0)
    
    model.fit(X_train, y_train, epochs=10, batch_size=64, verbose=2)
    
    # Probar el modelo con un conjunto de datos de prueba
    
    X_test = results.sample(frac=0.2).iloc[:, 0:6].values
    y_test = results.sample(frac=0.2).iloc[:, 7].values 
    y_pred = model.predict(Mes_Actual)
    
    # Medir la precisión del modelo

    y_pred = np.where(y_pred > 0.5, 1, 0)
    
    cm = confusion_matrix(y_test, y_pred)
    Accuracy = accuracy_score(y_test, y_pred)
    #Precision = precision_score(y_test, y_pred)
    #Recall = recall_score(y_test, y_pred)
    #F1_Score = f1_score(y_test, y_pred)

    return Accuracy, cm, y_pred


