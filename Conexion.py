#--Carga de librerias

from datetime import date, datetime, timedelta 

import pyodbc
import pandas as pd
import numpy as np
import warnings
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.cluster import KMeans
from scipy.stats import norm
import networkx as nx
from sklearn.preprocessing import StandardScaler
import Modelo
from tabulate import tabulate
from keras.models import Sequential
from keras.layers import Dense


from config import Credentials #-- Clase que contiene los credenciales de SQL

#-- Credenciales de SQL

server_ip = Credentials.DB_SERVER
bd_name = Credentials.DB_NAME
user_name = Credentials.DB_USERNAME
user_pass = Credentials.DB_PASSWORD

#-- Conexion con el servidos de SQL

conn = pyodbc.connect(f'DRIVER=ODBC Driver 17 for SQL Server; SERVER={server_ip}; DATABASE={bd_name}; UID={user_name}; PWD={user_pass}')


#-- Funciones 

def fn_fechas(hoy):
    fecha_inicio = (date(hoy.year-1, hoy.month, hoy.day) - timedelta(days=174)).strftime("%Y-%m-%d")
    fecha_fin = (date(hoy.year-1, hoy.month, hoy.day) - timedelta(days=113)).strftime("%Y-%m-%d")
    fecha_corte = (date(hoy.year-1, hoy.month, hoy.day) - timedelta(days=266)).strftime("%Y-%m-%d")
    return fecha_inicio, fecha_fin, fecha_corte    
                

#-- Utilizamos un Try para controlar y gestionar los errores y los procesos completados

try:
    with conn.cursor() as cursor:
        
    # -- Consulta desde la base de datos la tabla ###
        
        consulta = '''SELECT B.[IDN_Merchant], A.[_id], C.[IDN_Subsidiary], A.[transaction_date], D.[account_number], D.[IDN_User], A.[transaction_amount], E.[IDN_Transaction_type] FROM Transacciones A WITH(NOLOCK)
                            INNER JOIN DIM_Merchand B WITH(NOLOCK) ON A.merchant_id = B.merchant_id
                            INNER JOIN DIM_Subsidiary C WITH(NOLOCK) ON A.subsidiary = C.subsidiary
                            INNER JOIN DIM_User D WITH(NOLOCK) ON A.user_id = D.user_id 
                            INNER JOIN DIM_Transaction_type E WITH(NOLOCK) ON A.transaction_type = E.transaction_type'''
                                
        transaction = cursor.execute(consulta).fetchall()
        data = pd.DataFrame.from_records(transaction, columns=['IDN_Merchant', '_id', 'IDN_Subsidiary', 'transaction_date', 'account_number', 'IDN_User', 'transaction_amount', 'IDN_Transaction_type'])

        # Se consultan los datos de los maestros para actualizarlos en caso de que existan nuevas caracteristicas

        print("Consulta DIM_Merchand")
        consulta = "SELECT merchant_id FROM DIM_Merchand;"
        datos = cursor.execute(consulta).fetchall()
        df_tmp_merchant = pd.DataFrame.from_records(datos, columns=['merchant_id'])

        print("Consulta DIM_Subsidiary")
        consulta = "SELECT Subsidiary FROM DIM_Subsidiary;"
        datos = cursor.execute(consulta).fetchall()
        df_tmp_Subsidiary = pd.DataFrame.from_records(datos, columns=['Subsidiary'])
    
        print("Consulta DIM_User")
        consulta = "SELECT user_id FROM DIM_User;"
        datos = cursor.execute(consulta).fetchall()
        df_tmp_user_id = pd.DataFrame.from_records(datos, columns=['user_id'])

        print("Consulta DIM_Transaction_type")
        consulta = "SELECT Transaction_type FROM DIM_Transaction_type;"
        datos = cursor.execute(consulta).fetchall()
        df_tmp_Transaction_type = pd.DataFrame.from_records(datos, columns=['Transaction_type'])
        print(" \n")
            

        insert_merchant_id =   '''
                                MERGE INTO DIM_Merchand AS TARGET
                                USING (VALUES (?)) AS SOURCE (merchant_id)
                                ON TARGET.merchant_id = SOURCE.merchant_id
                                WHEN NOT MATCHED THEN
                                    INSERT (merchant_id) VALUES (SOURCE.merchant_id);
                            '''
        insert_Subsidiary =    '''
                                MERGE INTO DIM_Subsidiary AS TARGET
                                USING (VALUES (?)) AS SOURCE (Subsidiary)
                                ON TARGET.Subsidiary = SOURCE.Subsidiary
                                WHEN NOT MATCHED THEN
                                    INSERT (Subsidiary) VALUES (SOURCE.Subsidiary);
                            '''
        insert_user_id = '''
                                MERGE INTO DIM_User AS TARGET
                                USING (VALUES (?)) AS SOURCE (user_id)
                                ON TARGET.user_id = SOURCE.user_id
                                WHEN NOT MATCHED THEN
                                    INSERT (user_id) VALUES (SOURCE.user_id);
                            '''

        insert_Transaction_type = '''
                                MERGE INTO DIM_Transaction_type AS TARGET
                                USING (VALUES (?)) AS SOURCE (Transaction_type)
                                ON TARGET.Transaction_type = SOURCE.Transaction_type
                                WHEN NOT MATCHED THEN
                                    INSERT (Transaction_type) VALUES (SOURCE.Transaction_type);
                            '''


    #-- Información
    
        data.dtypes
        head_df = data.head()
        print(head_df)
        
        #-- Datos faltantes
        
        Datos_faltantes = pd.isna(data).sum()
        print(Datos_faltantes)
        
        #--- Análisis exploratorio
        
        transaction_type = pd.unique(data['transaction_type'])  # Solo existen 2 tipos de transacciones 
        sns.countplot(x=data['transaction_type']) 
        plt.show()
        
        merchant = pd.unique(data['merchant_id']) # Solo exsten 3 códigos unicos de comercios aliados
        sns.countplot(x=data['merchant_id']) 
        plt.show()     
        
        #--Formato de las variables
        data['transaction_amount'] = data['transaction_amount'].astype(float)
        
        ################################################################################
        
        #-- Agrego una nueva columna con un escalar = 1 para conocer posteriormente la cantidad de transacciones
        
        data['Transactions'] = 1 
        
        #filtro_2 = filtro[filtro['user_id'] == '6ba0bfc9c09d426909f8cd71217f9b39']
        
        # Ordenar las transacciones por fecha y agrupar por usuario
        
        transacciones = data.sort_values(['user_id', 'transaction_date'])
                
        # Agrupar las transacciones por usuario y por cada 24 horas
        
        grouped = transacciones.groupby(['user_id', pd.Grouper(key='transaction_date', freq='24H')])
        
        # Calcular la suma del monto por cada grupo
        
        result = grouped.agg({'transaction_amount': 'sum', 'Transactions': 'sum', 'merchant_id': 'first', 'transaction_type': 'first'})
        
        # Resetear el índice para mostrar las columnas agrupadas
        
        result = result.reset_index()
        
        ##############################################################################
        
        # Se actualizan los maestros en SQL

        if df_list_Merchand.empty:
            print("No se encontraron nuevos registros en df_data_Merchand")
        else: 
            print("Actualizando DIM_Merchand")
            cursor.fast_executemany = True
            cursor.executemany(insert_Merchand, df_list_Merchand.values.tolist())
            conn.commit()
                
        if df_list_Subsidiary.empty:
            print("No se encontraron nuevos registros en df_data_Subsidiary")
        else:
            print("Actualizando DIM_Subsidiary")
            cursor.fast_executemany = True
            cursor.executemany(insert_Subsidiary, df_list_Subsidiary.values.tolist())
            conn.commit()
        
        if df_list_user_id.empty:
            print("No se encontraron nuevos registros en df_data_user_id")
        else:    
            print("Actualizando DIM_user_id")
            cursor.fast_executemany = True
            cursor.executemany(insert_user_id, df_list_user_id.values.tolist())
            conn.commit()

        if df_list_Transaction_type.empty:
            print("No se encontraron nuevos registros en df_data_Transaction_type")
        else:    
            print("Actualizando DIM_Transaction_type")
            cursor.fast_executemany = True
            cursor.executemany(insert_Transaction_type, df_list_Transaction_type.values.tolist())
            conn.commit()

        ##############################################################################
        
        results = result.copy()
        
        # Normalizar el campo de transaction_amount
        
        scaler = StandardScaler()
        results['transaction_amount'] = scaler.fit_transform(results['transaction_amount'].values.reshape(-1, 1))
        results['Transactions'] = scaler.fit_transform(results['Transactions'].values.reshape(-1, 1))
        
        # Establecer umbral para transacciones anómalas
        
        Trans_atipical = np.mean(results['Transactions']) + (np.std(results['Transactions']) * 3) # Calculamos la media más 3 desviaciones
        Amount_atipical = np.mean(results['transaction_amount']) + (np.std(results['transaction_amount']) * 3)
        
        # Separo la información del mes actual para clasificarla posteriormente con el modelo
        
        Mes_Actual = results[results['transaction_date'] <= fn_fechas(date.today())[1]]
        
        relevant_vars = ['merchant_id', 'transaction_type', 'transaction_amount', 'Transactions']
        
        results = results[relevant_vars]
        Mes_Actual = Mes_Actual[relevant_vars]
        
        results[['merchant_id', 'transaction_type']] = results[['merchant_id', 'transaction_type']].astype('category')
        Mes_Actual[['merchant_id', 'transaction_type']] = Mes_Actual[['merchant_id', 'transaction_type']].astype('category')
        
        results = pd.get_dummies(results)
        Mes_Actual = pd.get_dummies(Mes_Actual)
        
        results['Decision'] = np.where((results['transaction_amount'] > Amount_atipical) & (results['Transactions'] > Trans_atipical) , 1, 0)
        

        ##--------------------------------------------------- Modelo de redes neuronales ------------------------------------------------
        
        Accuracy_RN, cm_RN, y_pred_RN = Red_Neuronal(results, Mes_Actual) #Agregar Modelo. #####SDFDGFFEADVWERGVWERGWREG
        print("Modelo de Redes Neuronales listo")
        
        ##-------------------------------------------------------- Random Forest --------------------------------------------------------
        
        Accuracy_RF, cm_RF, y_pred_RF = RandomForest(results, Mes_Actual)
        print("Modelo de Random Forest listo")
        
        ##--------------------------------------------- Análisis de patrones transaccionales -------------------------------------------
                
        Accuracy_AP, cm_AP,  y_pred_AP = Analisis_Patrones(results)
        print("Modelo de Patrones Transaccionales listo")    

        
        # Consolidamos los resultados de los tres modelos en una tabla y poder comparar facilmente los resultados
        
        table = [['Modelo', 'Accuracy'], 
                ['Random Forest', Accuracy_RF], 
                ['Analisis de patrones', Accuracy_AP],
                ['Redes Neuronales', Accuracy_RN]]
        
        print(table)
        #print(f"The best model is {Accuracy_RF}\n")

        ###############################################################################

        #Actualizacion de las tablas SQL

        consulta = "INSERT INTO Transacciones ('IDN_Merchant', '_id', 'IDN_Subsidiary', 'transaction_date', 'account_number', 'IDN_User', 'transaction_amount', 'IDN_Transaction_type') values(?,?,?,?,?,?,?,?,?,?,?,?)"
        cursor.fast_executemany = True 
        cursor.executemany(consulta, df.values.tolist())
        conn.commit()



except Exception as e:
    print("Error en procedimiento: ", e)
