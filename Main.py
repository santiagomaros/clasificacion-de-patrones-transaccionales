#--Carga de librerias

from datetime import date, datetime, timedelta 

import pyodbc
import pandas as pd
import numpy as np
import warnings
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.cluster import KMeans
from scipy.stats import norm
import networkx as nx
from sklearn.preprocessing import StandardScaler
import Modelo
from tabulate import tabulate
from keras.models import Sequential
from keras.layers import Dense


from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, accuracy_score


#-- Funciones 

def fn_fechas(hoy):
    fecha_inicio = (date(hoy.year-1, hoy.month, hoy.day) - timedelta(days=174)).strftime("%Y-%m-%d")
    fecha_fin = (date(hoy.year-1, hoy.month, hoy.day) - timedelta(days=113)).strftime("%Y-%m-%d")
    fecha_corte = (date(hoy.year-1, hoy.month, hoy.day) - timedelta(days=266)).strftime("%Y-%m-%d")
    return fecha_inicio, fecha_fin, fecha_corte    
                
#-- Utilizamos un Try para controlar y gestionar los errores y los procesos completados

try:
        
    data = pd.read_parquet('data/sample_data_0006_part_00.parquet', engine='pyarrow')
    
    #-- Información
    
    data.dtypes
    head_df = data.head()
    print(head_df)
    
    #-- Datos faltantes
    
    Datos_faltantes = pd.isna(data).sum()
    print(Datos_faltantes)
    
    #--- Análisis exploratorio
    
    transaction_type = pd.unique(data['transaction_type'])  # Solo existen 2 tipos de transacciones 
    sns.countplot(x=data['transaction_type']) 
    plt.show()
    
    merchant = pd.unique(data['merchant_id']) # Solo exsten 3 códigos unicos de comercios aliados
    sns.countplot(x=data['merchant_id']) 
    plt.show()     
    
    #--Formato de las variables
    data['transaction_amount'] = data['transaction_amount'].astype(float)
    
    ################################################################################
    
    #-- Agrego una nueva columna con un escalar = 1 para conocer posteriormente la cantidad de transacciones
    
    data['Transactions'] = 1 
     
    # Ordenar las transacciones por fecha y agrupar por usuario
    
    transacciones = data.sort_values(['user_id', 'transaction_date'])
            
    # Agrupar las transacciones por usuario y por cada 24 horas
    
    grouped = transacciones.groupby(['user_id', pd.Grouper(key='transaction_date', freq='24H')])
    
    # Calcular la suma del monto por cada grupo
    
    result = grouped.agg({'transaction_amount': 'sum', 'Transactions': 'sum', 'merchant_id': 'first', 'transaction_type': 'first'})
    
    # Resetear el índice para mostrar las columnas agrupadas
    
    result = result.reset_index()
    
    ##############################################################################
    
    results = result.copy()
    
    # Normalizar el campo de transaction_amount
    
    scaler = StandardScaler()
    results['transaction_amount'] = scaler.fit_transform(results['transaction_amount'].values.reshape(-1, 1))
    results['Transactions'] = scaler.fit_transform(results['Transactions'].values.reshape(-1, 1))
    
    # Establecer umbral para transacciones anómalas
    
    Trans_atipical = np.mean(results['Transactions']) + (np.std(results['Transactions']) * 3) # Calculamos la media más 3 desviaciones
    Amount_atipical = np.mean(results['transaction_amount']) + (np.std(results['transaction_amount']) * 3)
    
    # Separo la información del mes actual para clasificarla posteriormente con el modelo
    
    Mes_Actual = results[results['transaction_date'] >= fn_fechas(date.today())[1]]
    
    relevant_vars = ['merchant_id', 'transaction_type', 'transaction_amount', 'Transactions']
    
    results = results[relevant_vars]
    Mes_Actual = Mes_Actual[relevant_vars]
    
    results[['merchant_id', 'transaction_type']] = results[['merchant_id', 'transaction_type']].astype('category')
    Mes_Actual[['merchant_id', 'transaction_type']] = Mes_Actual[['merchant_id', 'transaction_type']].astype('category')
    
    results = pd.get_dummies(results)
    Mes_Actual = pd.get_dummies(Mes_Actual)
    
    results['Decision'] = np.where((results['transaction_amount'] > Amount_atipical) & (results['Transactions'] > Trans_atipical) , 1, 0)
    

    ##--------------------------------------------------- Modelo de redes neuronales ------------------------------------------------
    
    Accuracy_RN, cm_RN, y_pred_RN = Modelo.Red_Neuronal(results, Mes_Actual) 
    print("Modelo de Redes Neuronales listo")
    
    ##-------------------------------------------------------- Random Forest --------------------------------------------------------
    
    Accuracy_RF, cm_RF, y_pred_RF = Modelo.RandomForest(results, Mes_Actual)
    print("Modelo de Random Forest listo")
    
    ##--------------------------------------------- Análisis de patrones transaccionales -------------------------------------------
            
    Accuracy_AP, cm_AP,  y_pred_AP = Modelo.Analisis_Patrones(result, fn_fechas(date.today())[1])
    print("Modelo de Patrones Transaccionales listo")    
    
    # Consolidamos los resultados de los tres modelos en una tabla y poder comparar facilmente los resultados
    
    table = [['Modelo', 'Accuracy'], 
            ['Random Forest', Accuracy_RF], 
            ['Analisis de patrones', Accuracy_AP],
            ['Redes Neuronales', Accuracy_RN]]
    
    print(f" Matriz de confusion Red Neuronal {cm_RN}") 
    print(f" Matriz de confusion Random Forest {cm_RF}")
    print(f" Matriz de confusion Análisis de patrones {cm_AP}")
    
    print(table)
    #print(f"The best model is {Accuracy_RF}\n")

except Exception as e:
    print("Error en procedimiento: ", e)



        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        